# les problèmes principaux

## Sutherland

* Strategic: Difficult leveraging available resources, best practices are often deemed proprietary, are time consuming and difficult to maintain.

* Project and process management: Difficulty synchronizing work between distributed sites.

* Communication: 
* **Cultural**: Conflicting behaviors, processes, and

technologies.

* **Technical**: Incompatible data formats, schemas, and

standards.

* **Security**: Ensuring electronic transmission

confidentiality and privacy.

## Ebert

- communication
- workspace
- lifecycle
    - quality control

## back 2 basics
- managing the backlog is very challenging
- feedback loops lengthens

## Fowler offshore
- communication again
- offshore à l'opposé d'agile  (1èrement physiquement)
- problème d'intégration du travail effectué sur les sites différents
- cultural change (surtout les relations avec les supérieurs en Asie)
- feedback sur les fonctionnalités

## Ramesh
- communication need
- contrôle des processus
- accord formel/informel
- cohésion d'équipes

## Evolving distributed project management
- communication
- culture
- technique 
- sécurité