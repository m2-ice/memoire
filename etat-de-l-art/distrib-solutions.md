# Solutions principales

peut s'appliquer pas uniquement aux équipes dans différents pays mais par exemple à des équipes séparés dans des bureaux différents

## Sutherland

**Team orga**

Integrated Scrum teams 
un chef des PO et un chef SM pour les gros projets
Sprints 2 semaines

Obligation des dailys avec tout le monde même si les horaires sont compliqués
(exemple SirsiDynix et StarSoft : 17h45 Russie et 7:45 US )

La aussi les US doivent être le plus détaillées possible. 

balance entre formelle et informel ? 
 parce que Agile est très informel mais l'outsourcing nécessite une certaine base commune de connaissance

Les test fonctionnels sont réalisés après le planning et avant la moindre ligne de code. Les devs passent ensuite tous les UT et les test fonctionnels avant d'envoyer les modificiations sur les répository (dans ce cas SirsiDynix utilisent également l'automatisation des tests).

Pair programming uniquement si il y a un point très compliqué sur lequel un des dev bloque, mais est limité au strict minimum. 

Utilisation de JIRA --> vue en temps réel de l'état d'avancement de toutes les US
**Knowledge sharing**
des cycle courts sans limite de temps
QA team review every process during each iteration of offshore teams
créer des visites fréquentes, créer une culture d'équipe

## Ramesh 
(comparaisons des pratiques de 3 différentes enterprises)
Améliorer la communication : 
- synchroniser les heures de travails au maximum (faire varier les heures de standups pour les horaires difficiles changent d'équipe de temps en temps)
- régler le pb des formel/informel : un point de contact sur chaque site est en charge de gérer les communications
- communication **constante** : via SMS et autre outils de messagerie instantannée

Confiance : 
- visites fréquentes 
- team building 

très important pour créer une culture d'équipe, une cohésion, et ainsi la confiance

## Ebert

Outils communication : 
- instant messaging & VoIP & Conferencing & shared calendar are mandatory

Outils dev : 
- chaîne devops

Outils test :
- static  analy-sis  and  continuous  unit  test (devops)

Outil besoins et conception : 
- issue tracking system (Github, Gitlab, Jira, Pivotal Tracker), pour Kanban ou Scrum boards. Mêmes données dans (Evolving distributed)

## Nidiffer (evolving)

en plus d'être très pratique pour remplacer la présence physique, ce genre de logiciels de communications sont très peu cher en comparaison avec la mise en place de salle de visio conférence.

Management et changement culturel: 
- requiert de nouvelles compétences managériale. Le manager ne doit surtout pas rester dans le rôle du "dictateur hiérarchique" mais plutôt devenir le chef d'orchestre, le coordinateur des relations entre les membres de l'équipe.

## Fowler
Offshore agile 

- iteration plus courtes
- Tout le temps quelqu'un de l'autre côté pour favoriser la communication
- envoyer des embassadeurs de chaque équipe dans les autres pour favoriser la communication
- plus de ceremony/réunions pour forcer plus de communication 
    - cependant seulement 2 stand up par semaine dans l'étude de cas semblait suffire
- la aussi les tests fonct servent à définir les besoins
- travail préparatoire de division des stories en sous tâche pour réduire le temps des appels
- toutes les équipes font de l'analyse des besoins et de la conception, on divise dont le travail par fonctionnalité et non par géographie
- build régulier pour assurer un feebback rapide
- plus de documentation que dans une équipe classique, pour pallier le manque de communication.
    - aussi plus d'outils collaboratifs (issue tracking, wikis)



## Microsoft 

**chez microsoft** :
certaines équipes séparés en 5 zone temporelles

* sprints de 2 semaines.
* éviter les communications "informelles" &rarr; utiliser les story card comme chanel de comm.
* mélange scrum et XP
* PO, devs, testers, doc writers & experts, SM
* 4.3 million people in the USA work from home at least half the time

## challenge

distribution volontaire même si on sait que l'on va perdre en productivité.

supprimer le pair programming
**fuseau horaire** : communication encore plus difficile
trouver les horaires communs pour aligner l'équipe
**Effective distributed agile development** : minimiser l'impact de la distribution, focus sur le produit

## Les pratiques éprouvées

### communication
faire en sorte de favoriser les réunions impromptues, le matériel à dispo dans le bureau de l'équipe. Vidéo conférence plutôt que téléphone, partage facile des écrans.

logiciel de chat

webcams constamment ouverte pour relier 2 bureaux et savoir qui est là et qui ne l'est pas

remplacer les comm informelles par de la comm écrite (pour que tout le monde ai la même quantité d'info)

ajouter un créneau technique après la standup

## voyager

forcer les équipes à se rencontrer lors des premières itérations (avec des social events, renforcer la confiance et la connaissance les uns les autres). Eviter "Us vs Them"

## distribution de l'équipe

les fuseaux horaires ont bien plus d'impact que l'éloignement géographique.

Si aucun horaire en commun. Chaque sous équipe fait une daily, mais obligation de rester tart ou venir tôt pour faire le lien. Il est aussi possible d'inclure un "représentant" ayant travaillé dans l'autre partie de l'équipe, pour favoriser le lien (mais attention au trop de travail, fatigue, burnout... ).

## coaching de l'équipe

story cards doivent être très détaillées
remplacer le pair programming par les codes reviews

rechercher ce qui marche pour chaque équipe.
**adapter les recommantations de bases**

## distribution du travail

**ne pas distribuer géographiquement**
éviter de couper une US en sous tâche distribuée géographiquement --> citer aussi Nidiffer

## Long terme

**pour conclure**
les bonnes équipes distribuées sont souvent celles ayant effectué plusieurs projets.
essayer de conserver des "core members" pendant plusieurs releases.

## Les bons outils
à citer aussi avec les autres sources

online boards
fichiers partagés

## conclusion

important d'accepter la perte de performance et de chance su succès vis-à-vis de l'avantage financier que cela peut apporter.

# back to basics

**FAST use case** :

* Rule 1 : 
&rarr; après avoir mis le point équivalent de microsoft
éviter de customizer dès le départ. Se contenter de suivre la méthode de base et ne faire de changement qu'en cas de réel problème.(au moins 2 sprints avec le processus de base) 


* Rule 2 : 

utiliser un board partagé, comme Jira, pour permettre l'accès à la backlog n'importe quand
**facilite l'affichage de stats sur les sprints et les tâches (burndown). Aussi des outils non-tech, par exemple une horloge avec l'heure de l'autre fuseau horaire**

* Rule 3 durée: 

des sprints plus court pour forcer plus de communication lors des checkpoints de fin et de début de sprint
(la communication étant la bête noire des équipes distribuées)

* Rule 4 pratique ingénierie : 

UT, CI (channel de communication encore une fois), Integration Tests

* Rule 5 daily : 

one time to gather the team. Problème de fuseau horaire, certains "aujourd'hui" et d'autres "demain".

Mini scrum dans chaque équipe et une full daily, 

**co-habiter l'équipe au moins pour le 1er sprint**

* Rule 6 review : always demo retro

## conclusion

**The  main  reason  that  it  is  difficult  to  do  agile  with distributed   teams   is   that   distribution   can   reduce communication   bandwidth.**

le moins de modifications que possible