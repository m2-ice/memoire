
# Agile software development

 A lire pour une intro générale et recherches existante sur le thème agile en software dev au sens large

 agile est une réaction au méthodes traditionnelles. 

 ## what is agile dev
 selon Williams and Cockburn en 2003 : agile est une approche empirique et un processus non linéeaire.

 ## les recherches existantes (2010)

- 2002 Abrahamsson et al. at VTT : 10 différentes méthodes agiles. Agile ne couvrent pas toutes les phases de dev mais Agile couvre la gestion de projet. De plus agile est "effective and suitable for many situations and environments"

- 2004 Cohen et al. : pensent que Agile ne va pas remplacer les méthodes traditionnelles mais plutôt cohabiter avec

- 2005 Erickson et al. : XP est complexe à introduire dans des organisations complexes. Entreprises qui utilisent XP possèdent des employés moins stressés. 3 études sur 4 montres que XP augmente la productivité


# Overview 2 

36 études 76% XP. 

Hilkka et al. "old wine in new bottle" utilisation de pratiques similaires depuis plus de 10 ans
mais la plupart détermine agile dev comme étant quelque chose de nouveau.

- customer on site semble être l'indispensable de XP car il ressort de la majorité des études.
- fonctionne mieux avec des développeurs expérimentés, sinon beaucoup trouvent que c'est une perte de temps.

La majorité des développeurs expérimentés espère que leur organisation va continuer à utiliser agile. A côté de cela, des processus tel que le pair programming sont trèsépuisants pour les devs. 

les étudiants : enthousiaste, développement de compétences professionnelles comme la communication, l'engagement, la coopération et l'adaptabilité. (le pair programming ne semnle pas fonctionner lorsque la pair n'est pas d'un niveau équivalent)

Productivité : sur un ensemble de 4 études, comparant la production de projets similaires, 3 trouvent environ un gain de productivité de 45%, 1 trouve une perte de productivité de 44%