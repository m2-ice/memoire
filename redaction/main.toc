\babel@toc {french}{}
\contentsline {section}{Introduction}{3}{section*.1}% 
\contentsline {section}{\numberline {1}Objectif et contexte}{4}{section.1}% 
\contentsline {subsection}{\numberline {1.1}Agile au service des \IeC {\'e}quipes de d\IeC {\'e}veloppement}{4}{subsection.1.1}% 
\contentsline {subsection}{\numberline {1.2}La distribution des \IeC {\'e}quipes}{6}{subsection.1.2}% 
\contentsline {section}{\numberline {2}\IeC {\'E}tat de l'art}{8}{section.2}% 
\contentsline {subsection}{\numberline {2.1}L'efficacit\IeC {\'e} Agile}{8}{subsection.2.1}% 
\contentsline {subsection}{\numberline {2.2}Les limitations agile}{10}{subsection.2.2}% 
\contentsline {subsection}{\numberline {2.3}Probl\IeC {\`e}mes principaux des \IeC {\'e}quipes distribu\IeC {\'e}es}{13}{subsection.2.3}% 
\contentsline {subsubsection}{\numberline {2.3.1}communication}{14}{subsubsection.2.3.1}% 
\contentsline {subsubsection}{\numberline {2.3.2}culture}{14}{subsubsection.2.3.2}% 
\contentsline {subsubsection}{\numberline {2.3.3}technique et int\IeC {\'e}gration}{14}{subsubsection.2.3.3}% 
\contentsline {subsubsection}{\numberline {2.3.4}gestion de l'information et backlog}{14}{subsubsection.2.3.4}% 
\contentsline {subsubsection}{\numberline {2.3.5}processus et cycle de vie}{15}{subsubsection.2.3.5}% 
\contentsline {subsubsection}{\numberline {2.3.6}environnement de travail}{15}{subsubsection.2.3.6}% 
\contentsline {subsection}{\numberline {2.4}Solutions}{15}{subsection.2.4}% 
\contentsline {subsubsection}{\numberline {2.4.1}types d'\IeC {\'e}quipe}{15}{subsubsection.2.4.1}% 
\contentsline {subsubsection}{\numberline {2.4.2}sprints}{17}{subsubsection.2.4.2}% 
\contentsline {subsubsection}{\numberline {2.4.3}backlog}{17}{subsubsection.2.4.3}% 
\contentsline {subsubsection}{\numberline {2.4.4}les outils}{17}{subsubsection.2.4.4}% 
\contentsline {subsubsection}{\numberline {2.4.5}maintenir la qualit\IeC {\'e}}{19}{subsubsection.2.4.5}% 
\contentsline {subsubsection}{\numberline {2.4.6}am\IeC {\'e}liorer la communication}{19}{subsubsection.2.4.6}% 
\contentsline {subsubsection}{\numberline {2.4.7}am\IeC {\'e}lioration it\IeC {\'e}rative}{20}{subsubsection.2.4.7}% 
\contentsline {section}{\numberline {3}Analyse et comparaison}{20}{section.3}% 
\contentsline {subsection}{\numberline {3.1}Discussion}{20}{subsection.3.1}% 
\contentsline {subsubsection}{\numberline {3.1.1}apprentissages}{20}{subsubsection.3.1.1}% 
\contentsline {subsubsection}{\numberline {3.1.2}critiques}{21}{subsubsection.3.1.2}% 
\contentsline {subsection}{\numberline {3.2}Agile distribu\IeC {\'e} reste-t-il agile ?}{22}{subsection.3.2}% 
\contentsline {subsection}{\numberline {3.3}Points d'am\IeC {\'e}lioration}{23}{subsection.3.3}% 
\contentsline {section}{\numberline {4}Applications chez Air France - KLM}{24}{section.4}% 
\contentsline {subsection}{\numberline {4.1}G\IeC {\'e}n\IeC {\'e}ral}{24}{subsection.4.1}% 
\contentsline {subsection}{\numberline {4.2}Gestion de la crise}{25}{subsection.4.2}% 
\contentsline {subsection}{\numberline {4.3}Am\IeC {\'e}liorer les processus dans le groupe}{26}{subsection.4.3}% 
\contentsline {section}{Conclusion}{28}{section*.3}% 
\contentsline {section}{Table des figures}{29}{section*.4}% 
\contentsline {section}{Glossaire}{31}{section*.6}% 
\contentsline {section}{R\IeC {\'e}f\IeC {\'e}rences}{32}{section*.7}% 
